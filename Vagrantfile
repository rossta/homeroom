# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don"t touch unless you know what you"re doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  app_ip   = "10.254.254.254"
  app_path = "."

  mem  = `sysctl -n hw.memsize`.to_i / 1024 / 1024 / 4 # convert bytes to MB
  cpus = `sysctl -n hw.ncpu`.to_i

  # Source box
  # config.vm.box = "ubuntu1404-opsworks" # local base box
  #
  # Latest box
  config.vm.box = "rossta/ubuntu1404-opsworks"
  config.vm.box_version = "0.0.1"
  config.vm.boot_timeout = 30

  config.vm.network "private_network", ip: app_ip

  config.ssh.private_key_path = [ "~/.vagrant.d/insecure_private_key", "~/.ssh/id_rsa" ]
  config.ssh.forward_agent = true

  config.vm.synced_folder app_path, "/vagrant", type: "nfs"
  # Sync custom cookbooks
  config.vm.synced_folder "../opsworks-cookbooks", "/tmp/cookbooks", type: "nfs"

  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    vb.customize ["modifyvm", :id, "--memory", mem]
    vb.customize ["modifyvm", :id, "--cpus", cpus]
  end

  config.vm.define "app" do |layer|
    layer.vm.network "private_network", ip: app_ip

    layer.vm.provision :shell do |shell|
      shell.inline = "touch $1 && chmod 0440 $1 && echo $2 > $1"
      shell.args = %q{/etc/sudoers.d/root_ssh_agent "Defaults    env_keep += \"SSH_AUTH_SOCK\""}
    end

    layer.vm.provision :shell do |shell|
      shell.inline = "mkdir -p $1 && touch $2 && ssh-keyscan -H $3 >> $2 && chmod 600 $2"
      shell.args = %q{/root/.ssh /root/.ssh/known_hosts "github.com"}
    end

    layer.vm.provision "opsworks", type: "shell" do |shell|
      shell.path = "config/vagrant/opsworks"
      shell.args = [
        "config/vagrant/dna/opsworks.json",
        "config/vagrant/dna/app.json"
      ]
    end

    layer.vm.provision :shell do |shell|
      shell.inline = "cd /vagrant && bundle install"
    end
  end
end
