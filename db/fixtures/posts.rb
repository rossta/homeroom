Post.seed :id, {
  id: 1,
  title: "Toggle Rails Caching in Your RSpec Test Suite",
  description: "Using metadata for declarative test setup",
  published_at: Time.zone.parse("Dec 12, 2011"),
  slug: "toggle-rails-caching",
  body: (<<-MD)
A useful feature of RSpec is the ability to pass metadata to tests and suites. You may already be familiar with examples in [Capybara][1], such as passing `:js` to enable the javascript driver for a given spec. You may reach a point in the maturity of your test suite when it makes sense add your own configuration options.

 [1]: https://github.com/jnicklas/capybara

Once you introduce caching in your view layer, it can be easy for bugs to crop up around expiry logic. Since the Rails test environment ships with controller caching disabled, it may be useful to be able to toggle it on/off during the test run. To provide an optional caching mechanism for your specs, configure an around block:

```ruby
RSpec.configure do |config|
  config.around(:each, :caching) do |example|
    caching = ActionController::Base.perform_caching
    ActionController::Base.perform_caching = example.metadata[:caching]
    example.run
    ActionController::Base.perform_caching = caching
  end
end
```

The around block takes the RSpec example object as an argument. The block is triggered when :caching is detected as a key in an example’s metadata. The example object provides a number of methods for test introspection, allowing you to make changes before and after calling run to execute the spec. Here, we are storing the previously set value of `ActionContoller::Base.perform_caching`, setting it for the local suite, and setting it back to the original value after it completes.

As a result, we now have a simple, explicit mechanism for introducing caching to individual specs and suites:

```ruby
describe "visit the homepage", :caching => true do
  # test cached stuff
end
```
Happy testing.
  MD
}, {
  id: 2,
  title: "Ruby, You Autocomplete Me",
  description: "Hacking a smarter Ruby console",
  published_at: Time.zone.parse("Feb 5, 2014"),
  slug: "ruby-you-autocomplete-me",
  body: (<<-MD)
My team recently added a tagging feature to our web app. As the user types in
the text input, the app supplies autocomplete suggestions from our database via
javascript; a familiar UX. While backporting tags to existing records on the
`rails console`, it hit me: "Why not bring tag autocompletion to the command
line?"

The default `rails console` provides completion out-of-the-box though all the script
does is start `irb` with the rails environment and `irb/completion` required.

```ruby
#!/usr/bin/env ruby
require File.expand_path('../../load_paths', __FILE__)
require 'rails/all'
require 'active_support/all'
require 'irb'
require 'irb/completion'
IRB.start

# from https://github.com/rails/rails/blob/master/tools/console
```

Turns out that all `irb/completion` does is configure the ruby interface to the
[GNU Readline Library](http://cnswww.cns.cwru.edu/php/chet/readline/rltop.html).
This is done with the ruby [Readline](http://www.ruby-doc.org/stdlib-1.9.3/libdoc/readline/rdoc/Readline.html)
module. `Readline` accepts a `proc` that determines completion behavior by returning an array of string
candidates given an input string triggered, typically, by pressing `TAB`.

From `irb/completion`:

```ruby
if Readline.respond_to?("basic_word_break_characters=")
#  Readline.basic_word_break_characters= " \t\n\"\\'`><=;|&{("
  Readline.basic_word_break_characters= " \t\n`><=;|&{("
end
Readline.completion_append_character = nil
Readline.completion_proc = IRB::InputCompletor::CompletionProc
```
`IRB::InputCompletor::CompletionProc` is a proc that evaluates a large case
statement of regular expressions that attempt to determine the type of given
object and provide a set of candidates to match, such as `String` instance methods when
the input matches `$r{^((["']).*\2)\.([^.]*)$}`.

To give `Readline` a spin, fire up `irb` and paste in the following example, borrowed
from the [ruby docs](http://www.ruby-doc.org/stdlib-1.9.3/libdoc/readline/rdoc/Readline.html):

```ruby
require 'readline'

LIST = [
  'search', 'download', 'open',
  'help', 'history', 'quit',
  'url', 'next', 'clear',
  'prev', 'past'
].sort

comp = proc { |s| LIST.grep(Regexp.new(s))) }

Readline.completion_append_character = " "
Readline.completion_proc = comp
```

There's nothing stopping us from taking this to the `rails console` to take
advantage of our rails environment and even access the database. Building off
the example, we can replace the hard-coded array with a list of tags plucked
from a simple activerecord query:

```ruby
require 'readline'

comp = proc { |s| ActsAsTaggableOn::Tag.named_like(s).pluck(:name) }

Readline.completion_proc = comp
```
We have room for improvement. For one thing, this makes a new query every time
you attempt to autocomplete. For a reasonable number of tags, we could load the
tag list in memory and grep for the matches instead. There is still another problem;
by replacing the `Readline.completion_proc`, we've clobbered the functionality
provided by `irb/completion`. One approach would be to fall back to the
`IRB::InputCompletor::CompletionProc` or add its result to the array of candidates.
Given IRB has documented, [incorrect completions](https://github.com/cldwalker/bond#irbs-incorrect-completions)
(try completing methods on a proc) and no built-in support for extending completion behavior,
this could get messy.

Enter [bond](https://github.com/cldwalker/bond), a drop-in replacement for IRB
completion. It aims to improve on IRB's shortcomings and provides methods for
adding custom completions. To take advantage of Bond in the console:

```ruby
require 'bond'
Bond.start
```

Bond allows you to extend the strategies for autocompleting text with [the
`Bond.completion` method](https://github.com/cldwalker/bond/blob/master/lib/bond.rb#L21).
To set up a Bond completion, we need a condition and an action; when the condition is matched,
then the given action will determine which candidates are returned. Calling
`Bond.start` will register Bond's default completions. For example, the
following completion is triggered with the text for completion starts with a
letter preceded by "::"; the search space is scoped to `Object.constants`.

```ruby
# https://github.com/cldwalker/bond/blob/master/lib/bond/completion.rb#L13
complete(:prefix=>'::', :anywhere=>'[A-Z][^:\.\(]*') {|e| Object.constants }
```

To add tag autocompletion whenever we start a new string, we could use the following:

```ruby
include Bond::Search # provides methods to search lists

TAG_NAMES = ActsAsTaggableOn::Tag.pluck(:name) # load tag names in memory

Bond.complete(:name=>:tags, prefix: '"', :anywhere=>'([A-Z][^,]*)') {|e|
  tag = e.matched[2]
  normal_search(tag, TAG_NAMES)
}
```

Boom! Now we when autocomplete with some text inside an open double-quote, matching
tags from the database appear on the console.

```
irb(main)> "Face[TAB]
Face++                     Facebook Graph             FaceCash
Face.com                   Facebook Graph API         FaceDetection
Facebook                   Facebook Opengraph         Facelets
Facebook Ads               Facebook Real-time Updates Faces.com
Facebook Chat              Facebook SDK               Facetly
Facebook Credits           Facebook Social Plugins
irb(main)> "Facebook", "Twit[TAB]
Twitcher          TwitLonger        Twitter           Twitter Streaming Twitxr
TwitchTV          TwitPic           Twitter API       TwitterBrite
TwitDoc           TwitrPix          Twitter Bootstrap TwitterCounter
Twitgoo           Twitscoop         Twitter Grader    Twittervision
Twitlbl           TwitSprout        Twitter Oauth     Twitvid
```

Even though we ended up leveraging an existing gem, digging into the
Ruby standard library source code proved to be a useful exercise, revealing some
simple ways to hook into features easily taken for granted.
  MD
}, {
  id: 3,
  title: "Three Steps to Effective Code Reviews",
  description: "Exchanging feedback doesn&#8217;t have to be painful",
  published_at: Time.zone.parse("Feb 25, 2014"),
  slug: "effective-code-reviews",
  body: (<<-MD)
  These days, software developers are living in a [GitHub Workflow][gh-workflow]</a> world. They develop new code on version-controlled [branches][branches] and gather feedback prior to inclusion in the primary release, or “master” branch, through [pull requests][pull-requests].

  [gh-workflow]: http://scottchacon.com/2011/08/31/github-flow.html
  [branches]: http://git-scm.com/book/en/Git-Branching-Basic-Branching-and-Merging
  [pull-requests]: https://help.github.com/articles/using-pull-requests

  Our development team at ChallengePost has been using this workflow for almost two years with great success, although we&#8217;ve had our share of pain points. For better or worse, feedback typically happens asynchronously and is in written form. Convenient, yes, although this approach is not free of the wrinkles, especially when we use poor word choice, hyperbole, sarcasm, and other forms of counterproductive commentary.

  This has led to resentment and injured relationships on occasion. In response, I’m working to improve how we give and receive criticism.

  ### Building trust

  Let&#8217;s assume that, when done well, code reviews are a good thing. That is to say, the practice of giving and receiving feedback in a consistent, continual manner has true benefits. These may include improving code quality over time and driving convergence of ideas and practices within your team. In my experience, for feedback to be effective, trust amongst team members is a key requirement.

  This may not be an issue for teams that have been together for a long time or share common values, but for others, trust has to be earned. In the absence of trust, there&#8217;s more opportunity for personal differences to get intertwined with feedback. While there are no quick fixes, what follows are code review practices that we have adopted to foster our shared sense of trust.

  ### 1. Adopt a style guide

  **Spoiler alert**: code syntax and formatting are trivial choices. What&#8217;s most important is your team agrees on and adheres to a set of guidelines.

  Take a few hours as a team to hammer out a style guide for each of the languages you use. Better yet, use a public example like [GitHub&#8217;s style guide][style-guide] as a starting point. Besides the obvious benefits of consistency and maintainability, style guides reduce the likelihood of flared tempers during reviews; when you’re pushing to get a new feature out the door, it&#8217;s unhealthy to argue over whitespace. This works when your team respectfully follows and comments on style issues respectfully, saving concerns about existing guidelines for separate discussions.

  [style-guide]: https://github.com/styleguide

  ### 2. Start with the end in mind

  Imagine a developer who emerges, after hours or days off in the “zone,” with a sparkly new feature and asks for a review. All is good, right? Except that the rest of the team has issues with the implementation. Words are exchanged, the developer takes the feedback personally, and suddenly the entire team is distracted from shipping code.

  Personally, I believe code review should begin well before the final commit. It can happen early on; in short discussions with teammates once the ideas start to take shape. Get buy-in on your approach before you’re ready to merge your branch. Opening a pull request and asking for feedback while work is still in progress is a great way to build trust between teammates, and reduce the likelihood that criticism may be interpreted as a personal attack.

  ### 3. Use the Rubber Duck

  [Rubber duck][rubber-duck] debugging is a method of finding solutions simply by explaining code line-by-line to an inanimate object. We&#8217;ve found it helps to do the same with our writing, especially when our first instinct is to respond to code or another comment with sarcasm or anger. Take a moment to read your response aloud and question the wording, timing, and appropriateness. This includes taking into account the personality of the team members you’re addressing. [Thoughtbot][thoughtbot] has compiled a useful set of [code review guidelines][guidelines] to help both readers and writers respond thoughtfully. I also suggest that teammates share meta-feedback to ensure that everyone is hitting the right notes of tone and instruction.

  [rubber-duck]: http://en.wikipedia.org/wiki/Rubber_duck_debugging
  [thoughtbot]: http://thoughtbot.com
  [guidelines]: https://github.com/thoughtbot/guides/tree/master/code-review

  The next time you feel pain in a code review, take a step back to consider what’s missing. It could be that your team needs to adopt some guidelines to reduce friction and ensure feedback is exchanged in as a constructive and positive manner as possible. After all, you have both code and relationships to maintain.

  ### Resources

  * [Community ruby style guide](https://github.com/bbatsov/ruby-style-guide)
  * [GitHub style guide](https://github.com/styleguide)
  * [Code reviews: Good idea / bad idea?](http://mdswanson.com/blog/2012/11/04/code-reviews-good-idea-bad-idea.html)
  * [Why I Love Code Reviews](http://code.dblock.org/why-i-love-code-reviews)
  * [Code Review, Code Stories](http://whilefalse.blogspot.com/2012/06/code-reviews-code-stories.html)
  * [How we use pull requests to build GitHub](https://github.com/blog/1124-how-we-use-pull-requests-to-build-github)
  MD
}
