require "rails_helper"

RSpec.describe "Posts" do
  it "displays lists of posts" do
    create(:post, title: "Hello World", body: "Get busy living")
    create(:post, title: "Leveling Up", body: "Here are three steps to leveling up")

    visit "/"

    click_link "Archives"

    expect(page).to have_content("Hello World")
    expect(page).to have_content("Leveling Up")
  end
end
