FactoryGirl.define do
  factory :post do
    title "Functional JavaScript"
    description "How I learned to love the function"
    body { FFaker::Lorem.words(rand(450)+50) }
    published_at { 1.day.ago }
  end
end
