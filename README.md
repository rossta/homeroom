# AfterSchool Homeroom

You're just starting out your career as a web developer. Where do you begin?

I started out just like you. I followed tutorials - though they weren't as easy
to find in those days. We had big O'Reilly books and Learn Java in 5 Days.
If you've come out of a code school or dev bootcamp, you're probably way better
off than I was. But you'll still need guidance. You still get stuck. The
Homeroom helps you get unstuck.
