source "https://rubygems.org"

gem "rails", "4.2.1"
gem "pg"

gem "sass-rails", "~> 5.0"
gem "uglifier", ">= 1.3.0"
gem "coffee-rails", "~> 4.1.0"
gem "jquery-rails"
gem "foundation-rails"
gem "bower-rails"

gem "sdoc", "~> 0.4.0", group: :doc

gem "warden"
gem "bcrypt", "~> 3.1.7"

gem "unicorn"
gem "redis"
gem "dalli"

gem "sidekiq"

gem "pundit"
gem "virtus"
gem "rack-protection"
gem "simple_form"
gem "email_validator"
gem "recipient_interceptor"
gem "title"
gem "flutie"
gem "high_voltage"
gem "i18n-tasks"
gem "friendly_id"

group :development, :test do
  gem "factory_girl_rails"
  gem "ffaker"
  gem "pry-rails"
  gem "pry-rescue"
  gem "pry-byebug"
  gem "awesome_print"
  gem "dotenv-rails"
  gem "rubocop"
  gem "seed-fu"
end

group :development do
  gem "better_errors"
  gem "guard-bundler"
  gem "guard-rails"
  gem "quiet_assets"
  gem "rails_layout"
  gem "rb-fchange", require: false
  gem "rb-fsevent", require: false
  gem "rb-inotify", require: false
  gem "annotate"
  gem 'web-console', '~> 2.0'
  gem 'binding_of_caller'
end

group :test do
  gem "rspec-rails"
  gem "capybara"
  gem "shoulda-matchers"
  gem "database_cleaner"
  gem "launchy"
  gem "poltergeist"
  gem "formulaic"
  gem "timecop"
  gem "webmock"
  gem "vcr"
end

gem "newrelic_rpm", ">= 3.7.3"
